--------------------
ABOUT THIS DATASET
--------------------

This dataset contains data from a survey that was conducted among participants of the course 
"Research Data Management" at Bielefeld University in the years 2013 to 2017.

The survey was conducted online with the Unipark/Questback system. 128 students were invited by e-mail, 
of which 31 students partcipated in the survey.


--------------------
DATASET FILE LIST
--------------------

rdm-course_survey_questions.pdf   Codebook listing the questions and system variables
rdm-course_survey_results.csv      The survey data in CSV format. Columns are separated by semicolon.
rdm-course_survey_results.xlsx    The survey data in XLSX (MS Excel Workbook) format.
rdm-course_survey_comments.txt    Optional comments by participants of the survey.

--------------------
ANONYMIZATION
--------------------

The survey was done anonymous. In order to prevent any chance of de-anonymization, the semi-identifying data fields were randomized:
 + In which semester did you take the course?
 + What is your discipline?
 + What is your study programme?
 + How old are you?

The contents of the optional open text field "Comments" were extracted into a separate file "comments.txt" 


--------------------
LICENSE
--------------------

This work is licensed under the Creative Commons Attribution 4.0 International License. 
To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/.
A full text of the license is also included in the file LICENSE.txt.


--------------------
RELATED PUBLICATIONS
--------------------

For a detailed analysis of his data, please see:

  Wiljes, C. (2018). Survey Data about Research Data Management Course. 
  Bielefeld University. doi:10.4119/unibi/2920783
  http://dx.doi.org/10.4119/unibi/2920783 


---------------
ACKOWLEDGEMENTS
--------------- 

This work was supported by the German Research Foundation (DFG) and the Cluster of Excellence Cognitive Interaction Technology 'CITEC' (EXC 277) at Bielefeld University, which is funded by the German Research Foundation (DFG). 

